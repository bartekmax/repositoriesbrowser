//
//  Date.swift
//  RepositoriesBrowser
//
//  Created by Bartosz Koziel on 18/02/2019.
//  Copyright © 2019 Bartosz Koziel. All rights reserved.
//

import Foundation

extension Date {
    var dateString: String {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .none
        return "Updated on " + formatter.string(from: self)
    }
}

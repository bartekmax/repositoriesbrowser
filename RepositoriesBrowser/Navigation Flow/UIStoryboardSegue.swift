//
//  UIStoryboardSegue.swift
//  RepositoriesBrowser
//
//  Created by Bartosz Koziel on 20/02/2019.
//  Copyright © 2019 Bartosz Koziel. All rights reserved.
//

import UIKit

extension UIStoryboardSegue {
    func forward(repository: Repository) {
        (destination as? RepositoryDetailViewController)?.repository = repository
    }
}

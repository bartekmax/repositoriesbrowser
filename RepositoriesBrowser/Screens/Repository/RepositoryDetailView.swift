//
//  RepositoryDetailView.swift
//  RepositoriesBrowser
//
//  Created by Bartosz Koziel on 20/02/2019.
//  Copyright © 2019 Bartosz Koziel. All rights reserved.
//

import UIKit

protocol RepositoryDetailViewDelegate: class {
    func repositoryDetailViewButtonDidTap(_ view: RepositoryDetailView)
}

class RepositoryDetailView: UIView {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var languageLabel: UILabel!
    @IBOutlet private weak var starsLabel: UILabel!
    @IBOutlet private weak var forksLabel: UILabel!
    @IBOutlet private weak var licenseLabel: UILabel!
    @IBOutlet private weak var dateLabel: UILabel!
    
    weak var delegate: RepositoryDetailViewDelegate?
    
    var viewModel = ViewModel() {
        didSet {
            nameLabel.text = viewModel.name
            descriptionLabel.text = viewModel.description
            descriptionLabel.isHidden = viewModel.description.isEmpty
            languageLabel.text = viewModel.language
            starsLabel.text = viewModel.starText
            forksLabel.text = viewModel.forkText
            licenseLabel.text = viewModel.license
            dateLabel.text = viewModel.dateText
        }
    }
    
    @IBAction func show(_ sender: UIButton) {
        delegate?.repositoryDetailViewButtonDidTap(self)
    }
}

// MARK: - ViewModel
extension RepositoryDetailView {
    struct ViewModel {
        var name = ""
        var description = ""
        var language = ""
        var starCount = 0
        var forkCount = 0
        var license = ""
        var date = Date()
        
        var starText: String {
            return "\(starCount)"
        }
        
        var forkText: String {
            return "\(forkCount)"
        }
        
        var dateText: String {
            return date.dateString
        }
    }
}

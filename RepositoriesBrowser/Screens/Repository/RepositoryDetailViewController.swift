//
//  RepositoriesDetailViewController.swift
//  RepositoriesBrowser
//
//  Created by Bartosz Koziel on 15/02/2019.
//  Copyright © 2019 Bartosz Koziel. All rights reserved.
//

import Foundation
import SafariServices
import UIKit


class RepositoryDetailViewController: UIViewController {
    var repository: Repository?

    private var repositoryDetailView: RepositoryDetailView? {
        guard isViewLoaded else { return nil }
        
        let detailView = (view as! RepositoryDetailView)
        detailView.delegate = self
        return detailView
    }
}

// MARK: - ViewController
extension RepositoryDetailViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let localRepository = repository else { return }
        repositoryDetailView?.configure(with: localRepository)
    }
}

// MARK: - RepositoryDetailViewDelegate
extension RepositoryDetailViewController: RepositoryDetailViewDelegate {
    func repositoryDetailViewButtonDidTap(_ view: RepositoryDetailView) {
        guard let urlString = repository?.url, let url = URL(string: urlString) else { return }
        let safariController = SFSafariViewController(url: url)
        safariController.delegate = self
        
        present(safariController, animated: true)
    }
}

// MARK: - SFSafariViewControllerDelegate
extension RepositoryDetailViewController: SFSafariViewControllerDelegate {
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        dismiss(animated: true)
    }
}

// MARK: - RepositoryDetailView.ViewModel
extension RepositoryDetailView.ViewModel {
    init(repository: Repository) {
        name = repository.name
        description = repository.description ?? ""
        language = repository.language
        starCount = repository.stargazersCount
        forkCount = repository.forksCount
        license = repository.license?.name ?? ""
        date = repository.updateDate
    }
}

// MARK: - View Configurable
protocol RepositoryDetailConfigurable {
    func configure(with repository: Repository)
}

extension RepositoryDetailView: RepositoryDetailConfigurable {
    func configure(with repository: Repository) {
        viewModel = ViewModel(repository: repository)
    }
}

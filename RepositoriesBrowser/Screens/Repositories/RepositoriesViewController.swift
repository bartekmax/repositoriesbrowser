//
//  ViewController.swift
//  RepositoriesBrowser
//
//  Created by Bartosz Koziel on 15/02/2019.
//  Copyright © 2019 Bartosz Koziel. All rights reserved.
//

import UIKit

class RepositoriesViewController: UIViewController {
    @IBOutlet private weak var activityIndicatorView: UIActivityIndicatorView!
    @IBOutlet private weak var collectionView: UICollectionView! {
        didSet {
            let refreshControl = UIRefreshControl()
            refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
            collectionView.refreshControl = refreshControl
        }
    }
    
    private var dataSource: RepositoriesCollectionViewDataSource?
    private var selectedRepository: Repository?
    private var networkController = NetworkController()
    
    private var searchController: UISearchController? {
        didSet {
            searchController?.searchResultsUpdater = self
            searchController?.obscuresBackgroundDuringPresentation = false
            searchController?.searchBar.placeholder = "Search Repositories"
            navigationItem.searchController = searchController
            definesPresentationContext = true
        }
    }
    
    // MARK: - Methods
    @objc func refresh() {
        activityIndicatorView.startAnimating()
        networkController.fetchList(url: Endpoint.repositoriesURL, page: 1, count: 10) { [weak self] (results: [Repository]?) in
            results.map { self?.update(repositories: $0) }
        }
    }
}

// MARK: - ViewController
extension RepositoriesViewController {
    override func viewDidLoad() {
        super.viewDidLoad()

        searchController = UISearchController(searchResultsController: nil)
        refresh()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        collectionView.collectionViewLayout.invalidateLayout()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let repository = selectedRepository else { return }
        segue.forward(repository: repository)
    }
}

// MARK: - ViewController Private
private extension RepositoriesViewController {
    func update(repositories: [Repository]) {
        func setupDataSource() {
            dataSource = RepositoriesCollectionViewDataSource(repositories: repositories)
            collectionView.dataSource = dataSource
            collectionView.delegate = self
            collectionView.reloadData()
        }
        
        collectionView.refreshControl?.endRefreshing()
        activityIndicatorView.stopAnimating()
        setupDataSource()
    }
}

// MARK: - UICollectionViewDelegate
extension RepositoriesViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedRepository = dataSource?.item(at: indexPath)
        performSegue(withIdentifier: Segues.showDetailRepository, sender: nil)
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension RepositoriesViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        return CGSize(width: view.bounds.size.width, height: CGFloat(Row.height))
    }
}

// MARK: - UISearchResultsUpdating
extension RepositoriesViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        if let text = searchController.searchBar.text, !text.isEmpty, searchController.isActive {
            dataSource?.filter(contains: text)
        } else {
            dataSource?.showAll()
        }
        collectionView.reloadData()
    }
}

// MARK: - Row
extension RepositoriesViewController {
    struct Row {
        static let height = 80
    }
}

// MARK: - Segues
extension RepositoriesViewController {
    struct Segues {
        static let showDetailRepository = "ShowDetailRepository"
    }
}

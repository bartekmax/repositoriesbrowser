//
//  RepositoryCollectionViewDataSource.swift
//  RepositoriesBrowser
//
//  Created by Bartosz Koziel on 18/02/2019.
//  Copyright © 2019 Bartosz Koziel. All rights reserved.
//

import UIKit

class RepositoriesCollectionViewDataSource: NSObject {
    private var dataOrganizer: DataOrganizer
    
    init(repositories: [Repository]) {
        dataOrganizer = DataOrganizer(repositories: repositories)
    }
    
    func filter(contains text: String) {
        dataOrganizer.filter(contains: text)
    }
    
    func showAll() {
        dataOrganizer.showAll()
    }
    
    func item(at indexPath: IndexPath) -> Repository {
        return dataOrganizer.item(at: indexPath)
    }
}

// MARK: - UICollectionViewDataSource
extension RepositoriesCollectionViewDataSource: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataOrganizer.count()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: RepositoryCell = collectionView.dequeueReusableCell(for: indexPath)
        cell.configure(with: dataOrganizer.item(at: indexPath))

        return cell
    }
}

// MARK: - DataOrganizer
extension RepositoriesCollectionViewDataSource {
    struct DataOrganizer {
        private let repositories: [Repository]
        private var currentRepositories: [Repository]
        
        init(repositories: [Repository]) {
            self.repositories = repositories
            currentRepositories = repositories
        }
        
        mutating func filter(contains text: String) {
            currentRepositories = repositories.filter { $0.name.lowercased().contains(text.lowercased()) }
        }
        
        mutating func showAll() {
            currentRepositories = repositories
        }
        
        func item(at indexPath: IndexPath) -> Repository {
            return currentRepositories[indexPath.row]
        }
        
        func count() -> Int {
            return currentRepositories.count
        }
    }
}

// MARK: - Cell Configurable
protocol RepositoryConfigurable {
    func configure(with repository: Repository)
}

extension RepositoryCell: RepositoryConfigurable {
    func configure(with repository: Repository) {
        name = repository.name
    }
}

//
//  RepositoryCollectionViewCell.swift
//  RepositoriesBrowser
//
//  Created by Bartosz Koziel on 18/02/2019.
//  Copyright © 2019 Bartosz Koziel. All rights reserved.
//

import UIKit

class RepositoryCell: UICollectionViewCell {
    @IBOutlet private weak var nameLabel: UILabel!
    
    var name: String = "" {
        didSet {
            nameLabel.text = name
        }
    }
}

//
//  Repository.swift
//  RepositoriesBrowser
//
//  Created by Bartosz Koziel on 15/02/2019.
//  Copyright © 2019 Bartosz Koziel. All rights reserved.
//

import Foundation

struct ID: Decodable {
    let value: Int
}

extension ID {
    init(from decoder: Decoder) throws {
        let value = try decoder.singleValueContainer().decode(Int.self)
        self.init(value: value)
    }
}

struct Repository {
    struct License: Decodable {
        let name: String
    }
    
    let id: ID
    let name: String
    let description: String?
    let language: String
    let stargazersCount: Int
    let forksCount: Int
    let license: License?
    let updateDate: Date
    let url: String
}

extension Repository: Decodable {
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case description
        case language
        case stargazersCount = "stargazers_count"
        case forksCount = "forks_count"
        case license
        case updateDate = "updated_at"
        case url = "html_url"
    }
}

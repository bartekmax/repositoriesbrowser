//
//  NetworkRequest.swift
//  RepositoriesBrowser
//
//  Created by Bartosz Koziel on 18/02/2019.
//  Copyright © 2019 Bartosz Koziel. All rights reserved.
//

import Foundation

protocol NetworkRequest: class {
    associatedtype ModelType
    var urlRequest: URLRequest { get }
    var session: URLSession { get }
    var task: URLSessionDataTask? { get set }
    func deserialize(data: Data?, response: URLResponse?) -> ModelType?
}

extension NetworkRequest {
    func execute(withCompletion completion: @escaping (ModelType?) -> Void) {
        task = session.dataTask(with: urlRequest) { [weak self] (data, response, error) in
            completion(self?.deserialize(data: data, response: response))
        }
        task?.resume()
    }
}

// MARK: - JSONDataRequest
protocol JSONDataRequest: NetworkRequest where ModelType: Decodable {}

extension JSONDataRequest {
    func deserialize(data: Data?, response: URLResponse?) -> ModelType? {
        guard let localData = data else { return nil }
        
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
        return try? decoder.decode(ModelType.self, from: localData)
    }
}

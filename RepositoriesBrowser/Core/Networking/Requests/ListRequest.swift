//
//  ListRequest.swift
//  RepositoriesBrowser
//
//  Created by Bartosz Koziel on 18/02/2019.
//  Copyright © 2019 Bartosz Koziel. All rights reserved.
//

import Foundation

// MARK: - ListTypes
protocol ArrayType {}
extension Array: ArrayType {}

class ListRequest<ModelType: Decodable & ArrayType>: JSONDataRequest {
    let url: URL
    let session: URLSession
    let page: Int
    let count: Int
    var task: URLSessionDataTask?
    
    init(url: URL, session: URLSession, page: Int = 1, count: Int = 10) {
        self.url = url
        self.session = session
        self.page = page
        self.count = count
    }
}

// MARK: - NetworkRequest
extension ListRequest: NetworkRequest {
    var urlRequest: URLRequest {
        var request = URLRequest(url: url)
        guard page > 0, count > 0 else {
            return request
        }
        var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false)!
        urlComponents.queryItems = [URLQueryItem(name: Endpoint.FieldNames.page, value: "\(page)"),
                                    URLQueryItem(name: Endpoint.FieldNames.numberForPage, value: "\(count)")]
        request.url = urlComponents.url!
        return request
    }
}

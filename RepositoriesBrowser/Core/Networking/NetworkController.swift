//
//  NetworkController.swift
//  RepositoriesBrowser
//
//  Created by Bartosz Koziel on 18/02/2019.
//  Copyright © 2019 Bartosz Koziel. All rights reserved.
//

import Foundation

class NetworkController {
    private let session = URLSession(configuration: .default, delegate: nil, delegateQueue: .main)
    private var requests: [URL: AnyObject] = [:]
    private var task: URLSessionDataTask?
    
    // MARK: - Public methods
    func fetchList<Value: Decodable & ArrayType>(url: URL, page: Int, count: Int, withCompletion completion: @escaping (Value?) -> Void) {
        let request = ListRequest<Value>(url: url, session: session, page: page, count: count)
        requests[url] = request
        request.execute { [weak self] result in
            completion(result)
            self?.requests[url] = nil
        }
    }
}

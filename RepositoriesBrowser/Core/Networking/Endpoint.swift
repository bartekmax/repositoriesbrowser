//
//  Endpoint.swift
//  RepositoriesBrowser
//
//  Created by Bartosz Koziel on 18/02/2019.
//  Copyright © 2019 Bartosz Koziel. All rights reserved.
//

import Foundation

struct Endpoint {
    struct FieldNames {
        static let repositories = "repos"
        static let page = "page"
        static let numberForPage = "per_page"
    }
    
    static let apiRootURL = URL(string: "https://api.github.com/orgs/ApplauseOSS")!
    
    static var repositoriesURL: URL {
        return apiRootURL.appendingPathComponent(FieldNames.repositories)
    }
}

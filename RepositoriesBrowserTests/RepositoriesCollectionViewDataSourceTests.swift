//
//  RepositoriesCollectionViewDataSourceTests.swift
//  RepositoriesBrowserTests
//
//  Created by Bartosz Koziel on 21/02/2019.
//  Copyright © 2019 Bartosz Koziel. All rights reserved.
//

import XCTest
@testable import RepositoriesBrowser

class RepositoriesCollectionViewDataSourceTests: XCTestCase {
    typealias DataOrganizer = RepositoriesCollectionViewDataSource.DataOrganizer
    var repositories: [Repository]!
    var dataOrganizer: DataOrganizer!
    
    override func setUp() {
        repositories = DummyData().repositories
        dataOrganizer = DataOrganizer(repositories: repositories)
    }

    func testItems() {
        dataOrganizer.showAll()
        XCTAssertEqual(dataOrganizer.count(), 6)
        XCTAssertEqual(repositories[0].name, "Alamo")
        XCTAssertEqual(repositories[1].name, "Bravo")
        XCTAssertEqual(repositories[2].name, "Loko")
        XCTAssertEqual(repositories[3].name, "Onis")
        XCTAssertEqual(repositories[4].name, "Test")
        XCTAssertEqual(repositories[5].name, "Extension")
    }

    func testSearchingData() {
        dataOrganizer.filter(contains: "on")
        XCTAssertEqual(dataOrganizer.count(), 2)
        XCTAssertEqual(dataOrganizer.item(at: IndexPath(item: 0, section: 0)).name, "Onis")
        XCTAssertEqual(dataOrganizer.item(at: IndexPath(item: 1, section: 0)).name, "Extension")
    }
}

extension Repository {
    init(name: String) {
        self.init(id: ID(value: 1),
                  name: name,
                  description: nil,
                  language: "",
                  stargazersCount: 0,
                  forksCount: 0,
                  license: nil,
                  updateDate: Date(),
                  url: "")
    }
}

struct DummyData {
    let repositories: [Repository]
    
    init() {
        let repository1 = Repository(name: "Alamo")
        let repository2 = Repository(name: "Bravo")
        let repository3 = Repository(name: "Loko")
        let repository4 = Repository(name: "Onis")
        let repository5 = Repository(name: "Test")
        let repository6 = Repository(name: "Extension")
        
        repositories = [repository1, repository2, repository3,
                        repository4, repository5, repository6]
    }
}

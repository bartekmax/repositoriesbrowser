//
//  Helpers.swift
//  RepositoriesBrowserTests
//
//  Created by Bartosz Koziel on 21/02/2019.
//  Copyright © 2019 Bartosz Koziel. All rights reserved.
//

import Foundation
import XCTest
@testable import RepositoriesBrowser

extension XCTestCase {
    func loadDataFromFile<Result: Decodable>(name: String) -> Result? {
        guard let url = Bundle(for: type(of: self)).url(forResource: name, withExtension: "json"),
            let data = try? Data(contentsOf: url) else {
                XCTFail("Failed to load data")
                return nil
        }
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
        guard let model = try? decoder.decode(Result.self, from: data) else {
            XCTFail("Failed to decode JSON data")
            return nil
        }
        return model
    }
}

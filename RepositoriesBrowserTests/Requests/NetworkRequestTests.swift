//
//  NetworkControllerTests.swift
//  RepositoriesBrowserTests
//
//  Created by Bartosz Koziel on 21/02/2019.
//  Copyright © 2019 Bartosz Koziel. All rights reserved.
//

import XCTest
@testable import RepositoriesBrowser

class NetworkRequestTests: XCTestCase {
    func testRequest() {
        let session = PartialMockSession()
        let request = MockNetworkRequest(session: session)
        request.execute { _ in
            XCTAssertEqual(request.data, PartialMockDataTask.data)
            XCTAssertEqual(request.response, PartialMockDataTask.response)
        }
    }
}

// MARK: - MockDataTask
class PartialMockDataTask: URLSessionDataTask {
    static let data = "Test data".data(using: .utf8)
    static let response = URLResponse(url: URL(string: "testwebsite.com")!, mimeType: nil, expectedContentLength: 0, textEncodingName: nil)
    let completion: (Data?, URLResponse?, Error?) -> Void
    
    init(completion: @escaping(Data?, URLResponse?, Error?) -> Void) {
        self.completion = completion
    }
    
    override func resume() {
        completion(PartialMockDataTask.data, PartialMockDataTask.response, nil)
    }
}

class PartialMockSession: URLSession {
    override func dataTask(with request: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        return PartialMockDataTask(completion: completionHandler)
    }
}

class MockNetworkRequest: NetworkRequest {
    typealias ModelType = String
    var session: URLSession
    var task: URLSessionDataTask?
    var data: Data?
    var response: URLResponse?
    var urlRequest: URLRequest {
        return URLRequest(url: URL(string: "testwebsite.com")!)
    }
    
    init(session: URLSession) {
        self.session = session
    }
    
    func deserialize(data: Data?, response: URLResponse?) -> String? {
        self.data = data
        self.response = response
        return nil
    }
}

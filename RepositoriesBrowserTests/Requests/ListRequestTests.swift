//
//  ListRequestTests.swift
//  RepositoriesBrowserTests
//
//  Created by Bartosz Koziel on 21/02/2019.
//  Copyright © 2019 Bartosz Koziel. All rights reserved.
//

import XCTest
@testable import RepositoriesBrowser

class ListRequestTests: XCTestCase {
    func testUrlListRequest() {
        let url = URL(string: "https://test.com")!
        let listRequest = ListRequest<[String]>(url: url, session: URLSession.shared, page: 1, count: 10)
        
        XCTAssertEqual(listRequest.urlRequest.description, "https://test.com?page=1&per_page=10")
        
    }
}

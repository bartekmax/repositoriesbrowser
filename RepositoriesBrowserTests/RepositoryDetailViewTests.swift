//
//  RepositoryDetailViewTests.swift
//  RepositoriesBrowserTests
//
//  Created by Bartosz Koziel on 21/02/2019.
//  Copyright © 2019 Bartosz Koziel. All rights reserved.
//

import XCTest
@testable import RepositoriesBrowser

class RepositoryDetailViewTests: XCTestCase {
    func testViewModel() {
        var viewModel = RepositoryDetailView.ViewModel()
        viewModel.forkCount = 63
        viewModel.starCount = 112
        XCTAssertEqual(viewModel.forkText, "63")
        XCTAssertEqual(viewModel.starText, "112")
    }
}

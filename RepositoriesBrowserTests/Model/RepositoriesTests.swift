//
//  RepositoriesBrowserTests.swift
//  RepositoriesBrowserTests
//
//  Created by Bartosz Koziel on 15/02/2019.
//  Copyright © 2019 Bartosz Koziel. All rights reserved.
//

import XCTest
@testable import RepositoriesBrowser

class RepositoriesTests: XCTestCase {
    func testRepositoryDecoding() {
        let repositories: [Repository] = loadDataFromFile(name: "Repositories")!
        
        XCTAssertEqual(repositories.count, 10)
        XCTAssertEqual(repositories[0].id.value, 71135531)
        XCTAssertEqual(repositories[0].name, "kongfig")
        XCTAssertEqual(repositories[0].language, "JavaScript")
        XCTAssertEqual(repositories[0].stargazersCount, 0)
        XCTAssertEqual(repositories[0].forksCount, 0)
        XCTAssertEqual(repositories[0].license!.name, "MIT License")
    }
}

//
//  DateTests.swift
//  RepositoriesBrowserTests
//
//  Created by Bartosz Koziel on 21/02/2019.
//  Copyright © 2019 Bartosz Koziel. All rights reserved.
//

import XCTest
@testable import RepositoriesBrowser

class DateTests: XCTestCase {
    func testDateFormat() {
        let date = Date(stringDate: "21-02-2019")
        XCTAssertEqual(date?.dateString, "Updated on Feb 21, 2019")        
    }
}

extension Date {
    init?(stringDate: String) {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        guard let date = formatter.date(from: stringDate) else { return nil }

        self.init(timeIntervalSinceReferenceDate: date.timeIntervalSinceReferenceDate)
    }
    
}
